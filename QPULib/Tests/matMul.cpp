#include "hpc.h"
#include "QPULib.h"
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>
#include <strings.h>


void transpose(Ptr<Float>q, Ptr<Float> qT, Int n, Ptr<Int> loc_i)
{
  Int i, j;
  Int local_i = loc_i[me()<<4];//loc_i (0,5,10,15) inizia da 0,5,10
  Int final = loc_i[(me()+1)<<4];//arriva fino 5,10,15

  Ptr<Float> qTk = qT + index() + (me()<<4);
  Float qTval;
  
  // transpose q, storing the result in qT
  For (i=local_i, i<final-1, i++)
    gather(q+i);
    For (j=0, j<n, j++)
      gather(q+(j+1)*n+i);//valore per la prossima iterazione
      receive(qTval);
      store(qTval,qTk+i*n+j);
    End
    receive(qTval);//valore da buttare
  End
}



// Define function that runs on the GPU.
void QPU_matMul(Ptr<Float> p, Ptr<Float> qT, Int n, Ptr<Float> r, Ptr<Int> loc_i)
{
  Int i, j, k;
  Int local_i = loc_i[me()<<4];//loc_i (0,5,10,15) inizia da 0,5,10
  Int final = loc_i[(me()+1)<<4];//arriva fino 5,10,15 non compresi

  Ptr<Float> pk = p + index();
  Ptr<Float> qTk = qT + index();
  Ptr<Float> rk = r + index() + (me()<<4);

  Float qTval, pval;
  
  // multiply p and qT row-wise 
  For (i=local_i, i<final, i++)
    For (j=0, j<n, j++)

      //richiedo i valori per la prima iterazione con k=0
      gather(pk+i*n);
      gather(qTk+j*n);

      Float val=0.0f;
      For (k=0, k<n-NUM_LANES+1, k = k + NUM_LANES)
	gather(pk+i*n+k+NUM_LANES);gather(qTk+j*n+k+NUM_LANES);
	receive(pval);receive(qTval);
        val = val + pval * qTval;
      End
	
      //sommo tutti i valori all'interno del Float val
      val = val + rotate(val,8);
      val = val + rotate(val,4);
      val = val + rotate(val,2);
      val = val + rotate(val,1);

      store(val,rk+i*n+j);//r[i*n + j] = val;
      receive(pval);receive(qTval);//valori da buttare
    End
  End
}

int main(int argc, char* argv[])
{
  int n=512;

  if(argc>2){
    fprintf(stderr, "Usage: %s [n]\n", argv[0]);
    return EXIT_FAILURE;
  }

  if ( argc > 1 ) {
    n = atoi(argv[1]);
    if (n<16 || n%NUM_LANES){
      fprintf(stderr, "n deve essere maggiore di 15 e multiplo di %i\n",NUM_LANES);
      return EXIT_FAILURE;
    }
  }

  //variabili per il setup del programma
  const int QPUs = n > MAX_QPUS ? MAX_QPUS : n;
  const int lanesForQPUs = n/QPUs;
  int remaningLanes = n%QPUs;

  //matrici di grandezza n*n
  SharedArray<float> p(n*n), q(n*n);
  
  /*matrici estese per l'inserimento tramite QPUs
    se transizione viene eseguita tramite QPU qT(n*n+QPUs*NUM_LANES)*/
  SharedArray<float> r(n*n+QPUs*NUM_LANES),qT(n*n); 

  //vettore che assegna ad ogni QPUs la i di partenza
  SharedArray<int>  init_i((QPUs+1)*NUM_LANES);

  //assegno a ciascuna QPU il proprio numero di righe della matrice da eseguire
  for (int i = 1; i<QPUs+1; i++){
    int lanesActualQPU = lanesForQPUs + (remaningLanes-- > 0 ? 1 : 0) + init_i[(i-1)*NUM_LANES];
    for(int l = 0; l<NUM_LANES; l++){
      init_i[i*NUM_LANES+l] = lanesActualQPU;
    }
  }

  //setup matrici
  for ( int i=0; i<n; i++ ) {
    for ( int j=0; j<n; j++ ) {
      p[i*n+j] = (float)(i%10+j)/10.0f;
      q[i*n+j] = (float)(i%10+j)/10.0f;
    }
  }

  //compilo le funzioni  
  auto trasposizione = compile(transpose);
  trasposizione.setNumQPUs(QPUs);
  auto moltiplicazione = compile(QPU_matMul);
  moltiplicazione.setNumQPUs(QPUs);


  printf("\nMatrix size: %d x %d\n\n", n, n);

  //printf("funzione chiamata n: %i,QPUs: %i\n",n,QPUs);
  double tstart = hpc_gettime();

  //eseguo trasposizione
  //trasposizione(&q,&qT,n,&init_i);

  /*//parte seriale per rendere qT una matrice
  for (int qpu = 1; qpu < QPUs; qpu++) {
    for (int i = init_i[qpu*NUM_LANES]; i < init_i[(qpu+1)*NUM_LANES]; i++){
      for (int j=0; j < n; j++){
	//printf("%.1f ", qT[i*n+j+qpu*NUM_LANES]);
	qT[i*n+j]=qT[i*n+j+qpu*NUM_LANES];
      }
      //printf("\n");
    }
  }*/

  for (int i=0; i<n; i++) {
    //for(int qpu=0; qpu<QPUs; qpu++){ int i = init_i[(qpu+1)*NUM_LANES];
    for (int j=0; j<n; j++) {
      qT[j*n + i] = q[i*n + j];
    }
  }
  
  
  //moltiplicazione effettiva
  moltiplicazione(&p,&qT,n,&r,&init_i);

  //parte seriale per rendere r una matrice
  for (int qpu = 1; qpu < QPUs; qpu++) {
    for (int i = init_i[qpu*NUM_LANES]; i < init_i[(qpu+1)*NUM_LANES]; i++){
      for (int j=0; j<n;j++){
	//printf("%.1f ", r[i*n+j+qpu*NUM_LANES]);
	r[i*n+j] = r[i*n+j+qpu*NUM_LANES];
      }
      //printf("%.1f ", r[i*n+1]);
      //printf("\n");
    }
  }
  double tstop = hpc_gettime();
  printf("QPU transposed\tr[0][0] = %f, Execution time = %f\n", r[0], tstop - tstart);
  return 0;
}
