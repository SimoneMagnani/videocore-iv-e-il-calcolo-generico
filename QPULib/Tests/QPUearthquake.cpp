#include "hpc.h"
#include "QPULib.h"
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>

#define EMAX 4.0f
#define EDELTA 1e-4


float randab( float a, float b )
{
  return a + (b-a)*((float)rand() / (float)RAND_MAX);
}


// Define function that runs on the GPU.
void QPU_earthquake(Ptr<Float> grid, Ptr<Float>next, Ptr<Int> c,Int n,Float delta, Ptr<Float> sum, Int extra,Ptr<Int> loc_i)
{
  Float actualSum = 0;
  Float FDELTA = EMAX/4;
  Float EMAX2 = EMAX-delta;
  Int actualc = 0;
  Ptr<Float> gr = grid + index();
  Ptr<Float> nx = next + index();

  //il primo QPU parte dalla riga 1 (GhostCell)
  Int local_i = loc_i[me()<<4]+1;
  Int final = loc_i[(me()+1)<<4];

  gather(gr + local_i*n + 1);
  Float x,y;
  
  //creazione maschera per gli input non multipli di NUM_LANES
  Int imask = 1;
  Where (index()>=extra)
    imask = 0;
  End
  
  //itera su ogni riga
  For(Int i = local_i, i <= final, i++)

    //prende il primo elemento (non GC) della riga i
    receive(x);  

    //lavoro solo sulle celle per cui ho creato la maschera
    Where (imask > 0)

      //aumento
      x = x + delta;

      //calcolo di count
      Where (x > EMAX)
		actualc = actualc + 1;
      End
      	
      //propagazione energia considerando j=1
      Where (gr[(i  )*n  ] > EMAX2)
		x = x + FDELTA;
      End
      Where (gr[(i  )*n+2] > EMAX2)
		x = x + FDELTA;
      End
      Where (gr[(i-1)*n+1] > EMAX2)
        x = x + FDELTA;
      End
      Where (gr[(i+1)*n+1] > EMAX2)
        x = x + FDELTA;
      End

      //update del valore
      Where (x > EMAX)
        x = x - EMAX;
      End
	
      //calcolo sum
      actualSum = actualSum + x;
      
    End

    gather(gr+ i*n+(extra+1));// j=extra+1(GhostCell)
    store(x,nx+i*n+1);// j=1

    
    //stesse operazioni di prima a partire da gr+extra
    For(Int j = extra+1, j < n-1, j = j+NUM_LANES)
      

      receive(x);

    /*nel caso precedente non era possibile la gather/receive non bloccante 
      in quanto all'interno di un Where*/
      gather(gr+(i  )*n+(j-1));
      gather(gr+(i  )*n+(j+1));
      gather(gr+(i-1)*n+(j  ));   
      gather(gr+(i+1)*n+(j  ));

      //aumento
      x = x + delta;

      //calcolo di count
      Where (x > EMAX)
		actualc = actualc + 1;
      End

      //propagazione energia
      receive(y);
      Where (y > EMAX2)
		x = x + FDELTA;
      End
      receive(y);
      Where (y > EMAX2)
		x = x + FDELTA;
      End
      receive(y);
      Where (y > EMAX2)
		x = x + FDELTA;
      End
      receive(y);
      Where (y > EMAX2)
		x = x + FDELTA;
	  End

      //prossima iterazione j += NUM_LANES a parte se finisce la riga
      gather(gr+i*n+j+NUM_LANES);
      
      //update del valore
      Where (x > EMAX)
		x = x - EMAX;
      End

      //calcolo sum
      actualSum = actualSum + x;

      store(x, nx+i*n+j);
    End

    gather(gr + (i+1)*n + 1);//valore per la prossima iterazione
    receive(x);//valore da buttare
   
  End

  //calcolo il valore di sum per questa QPU
  actualSum = actualSum+rotate(actualSum,8);
  actualSum = actualSum+rotate(actualSum,4);
  actualSum = actualSum+rotate(actualSum,2);
  actualSum = actualSum+rotate(actualSum,1);

  //calcolo il valore di c per questa QPU
  actualc = actualc+rotate(actualc,8);
  actualc = actualc+rotate(actualc,4);
  actualc = actualc+rotate(actualc,2);
  actualc = actualc+rotate(actualc,1);

  
  //ogni QPU ha un valore di c e sum che vengono messi in sum/c[QPU*16]
  store(actualSum, sum+(index() + (me()<<4)));
  store(actualc  , c  +(index() + (me()<<4)));
 
  //ultimo valore da buttare
  receive(x);
}

int main(int argc, char* argv[])
{
  // Construct kernel
  int n=1024, nsteps = 2048, c=0;
  float s=0.0;
  srand(19);

  if(argc>3){
    fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
    return EXIT_FAILURE;
  }

  if ( argc > 1 ) {
    nsteps = atoi(argv[1]);
  }

  if ( argc > 2 ) {
    n = atoi(argv[2]);
    if (n<16){
      fprintf(stderr, "n deve essere maggiore di 15\n");
      return EXIT_FAILURE;
    }
  }

  //variabili per il setuo del programma
  const int QPUs = n > MAX_QPUS ? MAX_QPUS : n;
  const int lanesForQPUs = n/QPUs;
  int remaningLanes = n%QPUs;
  const int extra = n%NUM_LANES;
  const int totCells = n*n;
  n += 2;
  //printf("%i, %i\n", lanesForQPUs, remaningLanes);

  //consideriamo anche lo spazio per le ghost cell
  SharedArray<float> cur(n*n), next(n*n), sum(QPUs*NUM_LANES);
  SharedArray<int> count(QPUs*NUM_LANES), init_i((QPUs+1)*NUM_LANES);

  //calcolo quante righe deve calcolare ogni QPU
  for (int i = 1; i<QPUs+1; i++){
    int lanesActualQPU = lanesForQPUs + (remaningLanes-- > 0 ? 1 : 0) + init_i[(i-1)*NUM_LANES];
    printf("%i: %i\n",i,lanesActualQPU);
    for(int l = 0; l<NUM_LANES; l++){
      init_i[i*NUM_LANES+l] = lanesActualQPU;
    }
  }

  //setup matrice senza le GC
  for ( int i=1; i<n-1; i++ ) {
    for ( int j=1; j<n-1; j++ ) {
      cur[i*n+j] = randab(0, EMAX*0.1f);
    }
  }

  
  SharedArray<float> *tmp, *pcur=&cur, *pnext=&next;

  auto earthquake = compile(QPU_earthquake);
  earthquake.setNumQPUs(QPUs);
  const double tstart = hpc_gettime();
  //printf("funzione chiamata n: %i,QPUs: %i, extra: %i \n",n,QPUs,extra);
  
  for (int i=0; i<nsteps;i++){
    earthquake(pcur, pnext, &count, n, (float)EDELTA, &sum, extra, &init_i);
    for(int stepsSum=0;stepsSum<QPUs;stepsSum++){
      c += count[stepsSum*NUM_LANES];
      s += sum  [stepsSum*NUM_LANES];
    }
    printf("%d %f\n", c, s/(float)totCells);
    c=0;s=0;
    tmp = pnext;
    pnext = pcur;
    pcur = tmp;
    /*//funzione di debug per le prime iterazioni 
      for (int i = 0; i < n*n; i++) {
      if(cur[i] != 0){
	float shouldberight = (*pnext)[i]+(float)EDELTA;
	float rightness = 0.00001f;
	if (!((shouldberight + rightness > (*pcur)[i])
	      && (shouldberight - rightness < (*pcur)[i])))
	  printf("n:%f, cur:%f\n", (*pnext)[i]+(float)EDELTA, (*pcur)[i]);
      } else if (next[i] != 0) printf("modificato qualcosa nelle ghostcell");
    }*/
  }
  const double elapsed = hpc_gettime()-tstart;

  double Mupdates = (((double)n)*n/1.0e6)*nsteps; /* milioni di celle aggiornate per ogni secondo di wall clock time */
  fprintf(stderr, "%s : %.4f Mupdates in %.4f seconds (%f Mupd/sec)\n", argv[0], Mupdates, elapsed, Mupdates/elapsed);
    
  return 0;
}
