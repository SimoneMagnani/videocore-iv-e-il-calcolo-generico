/**
 * Simone Magnani
 * Matricola: 793493
 * OMP
*/
/****************************************************************************
 *
 * earthquake.c - Simple 2D earthquake model
 *
 * Copyright (C) 2018 Moreno Marzolla <moreno.marzolla(at)unibo.it>
 * Last updated on 2018-12-29
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 * Versione di riferimento del progetto di High Performance Computing
 * 2018/2019, corso di laurea in Ingegneria e Scienze Informatiche,
 * Universita' di Bologna. Per una descrizione del modello si vedano
 * le specifiche sulla pagina del corso:
 *
 * http://moreno.marzolla.name/teaching/HPC/
 *
 * Per compilare:
 *
 * gcc -D_XOPEN_SOURCE=600 -std=c99 -Wall -Wpedantic earthquake.c -o earthquake
 *
 * (il flag -D_XOPEN_SOURCE=600 e' superfluo perche' viene settato
 * nell'header "hpc.h", ma definirlo tramite la riga di comando fa si'
 * che il programma compili correttamente anche se inavvertitamente
 * non si include "hpc.h", o per errore non lo si include come primo
 * file come necessario).
 *
 * Per eseguire il programma si puo' usare la riga di comando seguente:
 *
 * ./earthquake 100000 256 > out
 * ./omp-earthquake 100000 256 > out
 *
 * Il primo parametro indica il numero di timestep, e il secondo la
 * dimensione (lato) del dominio. L'output consiste in coppie di
 * valori numerici (100000 in questo caso) il cui significato e'
 * spiegato nella specifica del progetto.
 *
 ****************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>

/* energia massima */
#define EMAX 4.0f
/* energia da aggiungere ad ogni timestep */
#define EDELTA 1e-4

/**
 * Restituisce un puntatore all'elemento di coordinate (i,j) del
 * dominio grid con n colonne.
 */
static inline float *IDX(float *grid, int i, int j, int n)
{
    return (grid + i*n + j);
}

/**
 * Restituisce un numero reale pseudocasuale con probabilita' uniforme
 * nell'intervallo [a, b], con a < b.
 */
float randab( float a, float b )
{
    return a + (b-a)*(rand() / (float)RAND_MAX);
}

/**
 * Inizializza il dominio grid di dimensioni n*n con valori di energia
 * scelti con probabilità uniforme nell'intervallo [fmin, fmax], con
 * fmin < fmax.
 *
 * NON PARALLELIZZARE QUESTA FUNZIONE: rand() non e' thread-safe,
 * qundi non va usata in blocchi paralleli OpenMP; inoltre la funzione
 * non si "comporta bene" con MPI (i dettagli non sono importanti, ma
 * posso spiegarli a chi e' interessato). Di conseguenza, questa
 * funzione va eseguita dalla CPU, e solo dal master (se si usa MPI).
 */
void setup( float* grid, int n, float fmin, float fmax )
{
  const int limit = n-1;
    for ( int i=1; i<limit; i++ ) {
        for ( int j=1; j<limit; j++ ) {
            *IDX(grid, i, j, n) = randab(fmin, fmax);
        }
    }
}

/**
 * inizializzazione di un bordo largo 1 di ghost cell
 * per una qualsiasi matrice il cui spazio è già allocato
 */
void setupGC(float *grid, int n)
{
  const int limit = n-1;
  for (int i=0; i<n; i++){
    *IDX(grid, i, 0, n) = 0;
    *IDX(grid, 0, i, n) = 0;
    *IDX(grid, limit, i, n) = 0;
    *IDX(grid, i, limit, n) = 0;
      
  }
}

/**
 * La funzione riassume tutto l'algoritmo.
 * Inizialmente viene sommato delta e poi vengono contate le 
 * celle con energia strettamente maggiore di EMAX;
 * dopodiché per poter parallelizzare il tutto, il confronto viene
 * fatto con le celle di grid (non ancora aggiornate) e controllando
 * che siano maggiori di EMAX2 (EMAX-delta).
 * Viene poi aggiornata la cella di next corrispondente
 * e vengono calcolati i parametri da ritornare (c e Emean) 
 */
float increment_energy_and_count_and_propagate( float *grid, int n, float delta, float *next, int *c )
{
  float sum = 0.0f;
  const int limit = n-1;
  const float FDELTA = EMAX/4;
  const float EMAX2 = EMAX-delta;
  int local_c = 0;
#pragma omp parallel for default(none) shared(delta,grid,n,next) reduction(+:local_c) reduction(+:sum)
  for (int i=1; i<limit; i++) {
    for (int j=1; j<limit; j++) {
      float F = *IDX(grid,i,j,n);

      F += delta;

      if (F > EMAX ) {local_c++; }

      if ((*IDX(grid, i, j-1, n) > EMAX2)) { F += FDELTA;}
       
      if ((*IDX(grid, i, j+1, n) > EMAX2)) { F += FDELTA;}
            
      if ((*IDX(grid, i-1, j, n) > EMAX2)) { F += FDELTA;}

      if ((*IDX(grid, i+1, j, n) > EMAX2)) { F += FDELTA;}

      if(F>EMAX) {
	F-=EMAX;
      }
	  
      *IDX(next, i, j, n) = F;
      sum += F;
    }
  }
  *c = local_c;
  return sum  / ((n-2)*(n-2));
}

int main( int argc, char* argv[] )
{
    float *cur, *next;
    int s, n = 256, nsteps = 2048;
    float Emean;
    int c;

    srand(19); /* Inizializzazione del generatore pseudocasuale */
    
    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        n = atoi(argv[2]);
    }
    
    /* consideriamo anche lo spazio per le ghost cell*/
    n += 2;
    
    const size_t size = n*n*sizeof(float);

    /* Allochiamo i domini */
    cur = (float*)malloc(size); assert(cur);
    next = (float*)malloc(size); assert(next);

    /* L'energia iniziale di ciascuna cella e' scelta 
       con probabilita' uniforme nell'intervallo [0, EMAX*0.1] */       
    setup(cur, n, 0, EMAX*0.1);
    /* Inizializziamo le ghost cell per entrambe le matrici*/
    setupGC(cur, n);
    setupGC(next, n);
    
    const double tstart = hpc_gettime();
    for (s=0; s<nsteps; s++) {
      /* L'ordine delle istruzioni che seguono e' importante */
      Emean = increment_energy_and_count_and_propagate(cur, n, EDELTA, next, &c);
      printf("%d %f\n", c, Emean);
	
      float *tmp = cur;
      cur = next;
      next = tmp;
    }
    const double elapsed = hpc_gettime() - tstart;
    
    double Mupdates = (((double)n)*n/1.0e6)*nsteps; /* milioni di celle aggiornate per ogni secondo di wall clock time */
    fprintf(stderr, "%s : %.4f Mupdates in %.4f seconds (%f Mupd/sec)\n", argv[0], Mupdates, elapsed, Mupdates/elapsed);

    /* Libera la memoria */
    free(cur);
    free(next);
    
    return EXIT_SUCCESS;
}
